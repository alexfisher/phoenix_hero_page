<?php
/**
 * @file
 * phoenix_hero_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function phoenix_hero_page_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hero_page_titles|node|hero_page|default';
  $field_group->group_name = 'group_hero_page_titles';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'hero_page';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Titles',
    'weight' => '1',
    'children' => array(
      0 => 'title_field',
      1 => 'field_hero_page_subtitle',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Titles',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-hero-page-titles field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_hero_page_titles|node|hero_page|default'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Titles');

  return $field_groups;
}
