<?php
/**
 * @file
 * phoenix_hero_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phoenix_hero_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function phoenix_hero_page_node_info() {
  $items = array(
    'hero_page' => array(
      'name' => t('Hero Page'),
      'base' => 'node_content',
      'description' => t('A basic page with a hero image at the top.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
